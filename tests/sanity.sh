#!/bin/bash
#
# This is a simple sanity test to satisfy the RHEL8.1 onboard gating
# requirement.

ib_atomic_bw --version
ret=$?

# for reasons I have not investigated, at least on hosts without any rdma
# hardware, the above returns 1 instead of 0 (as does --help)
if [ "$ret" -eq 1 ]; then
  exit 0
fi

exit $ret
